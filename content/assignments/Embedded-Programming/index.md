+++
title = "Embedded Programming"
+++


### Solder header pins on xiao RP2040 board
![a1.jpg](a1.jpg)

![A02.jpg](A02.jpg)

![acc.jpg](acc.jpg)


when I was soldering the board, to make sure I didn't bridge two pins, I used Electrical test meters. 
it was ringing when I test, so I meltedd the material again and adjusted the position to make they were divided. then I finished the soldering.


### Program the XIAO board to light the R,G and B LEDs, NEOPIXEL LED).

I tried making the green light blink. In this step, setting the right board and port in the tool is important. 


![a2q.png](a2q.png)


{{<video src="a01.mp4">}}This video is about blinking the green led.{{</video>}}


![a3.jpg](a3.jpg)


{{<video src="a3.mp4">}}
Then I finished the blinking of R, G, B led.
{{</video>}}

When I was using the button to control the NEOPIXEL LED, I got two problems.

![a3q.png](a3q.png)


The first one was "Low" was not declared in this scope,
The reason is it should be "LOW", its capital form.
The another one was the NEOPIXEL can only show red color.
That's also because my typo, int g = random(0.255), that was wrong, because the range should be 0 to 255, I should use comma, so that the value in this range  could be changed randomly.

![a5.png](a5.png)

![a6.png](a6.png)

{{<video src="a5.mp4">}} {{</video>}}

### Write code to send to and receive messages from the board using the Serial class.
![a7.png](a7.png)


![a9.png](a9.png)


![a10.png](a10.png)



### Use the mini-breadboard, two buttons to create a simple circuit and write code to detect buttons press.

Once I was done with one button to control the NEO pixel, I tried another button to do the same for the neopixel led.

I Connected another button to pin D8

The circuit picture is as follows

![a11.jpg](a11.jpg)

I added the code int btnPin2 = D8; pinMode(btnPin2, INPUT_PULLUP); int btnState2 = digitalRead(btnPin2);
and also I added  if (btnState1 ==LOW) this state.

![a12.png](a12.png)

![a13.png](a13.png)

Finally, both button 1 and button 2 can control the Neopixel LED, Every time I push a button, neopixel switches colors.


{{<video src="a15.mp4">}} {{</video>}}


I learned how to control LEDs with a button and also the basic code of serial in Xiao Examples. 

I first tried to control neopixel with button 1, control the LED with button 2, then I tried serial communication, when I send NEO to the serial, the NEO pixel lights up R, G, B in turn, and when I send LED, the LED blink in red blue and green in sequence.




![aa13.png](aa13.png)

![aa14.png](aa14.png)

{{<video src="a18.mp4">}} {{</video>}}



![a15.png](a15.png)


![a16.png](a16.png)

![a17.png](a17.png)



{{<video src="a19.mp4">}} {{</video>}}





