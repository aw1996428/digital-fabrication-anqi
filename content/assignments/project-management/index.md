+++
title = "Project Management"
+++

This week we still improve our websites and learn about media optimization.



### SSH Keys

At first, I thought that I fixed this problem, but actually I didn’t, and that’s why I have to provide the user name and personal token every time, then I follow the documentation with 
https://docs.github.com/zh/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account
I found I have to do the process again to check which part I missed. After generated a new SSH key and Added the SSH key to the ssh-agent. At last I added the SSH key to my GitHub account. 


![a01.png](a01.png)
![a02.png](a02.png)
![a03.png](a03.png)





### HTML Coding

I read the relevant syntax of HTML then I get to know how to design Navigation bar, set margin, input images and adjust their width and height. 
I got documentaion of HTML tags from https://www.w3schools.com/TAgs/tag_html.asp
https://www.w3schools.com/TAgs/tag_img.asp

https://www.w3schools.com/TAgs/tag_ul.asp

https://developer.mozilla.org/en-US/docs/Web/HTML/Element

![a04.png](a04.png)





![a05.png](a05.png)


### Using git to upload the files to Gitlab

![02.jpg](02.jpg)

I Used a credit card to sign in then went to the ci cd page to download the template
and to commit to the change.

Following the lesson by Kris,
The three command is essential for uploading
They are git add .  git commit -m “hugo” and git push

![a06.jpg](a06.jpg)




![a07.jpg](a07.jpg)


Later, while documenting project management, I edited directly in Gitlab, the index.md page





### Install Hugo

I met a problem with the Hugo command not being found
I re-download Extended zip, added the environment variable which is where the software is located. the problem fixed

### Hugo Coding

![3.jpg](3.jpg)

I found that the local webpage showing something wrong, the reason wass I didn’t run hugo serve in gitbash. In the coding experience, I also met error with the typo of the direction of “.”
After carefully read the document of https://gohugo.io/variables/site/, I had more understanding of the grammar, and then avoided this error.

### Using git to upload the files to gitlab

When I first finished deploying, my webpage didn’t show contents.
It showed４０４ page not found, The reason was that URL was not matched,
Then I edited the nav html and added the repo location. The problem fixed. 

![a11.png](a11.png)


### Install ImageMagick

![04.png](04.png)

At first, I can not install Imagemagick, and got the error 5, that because the permission problems. I googled how to solve this. It showed that I need to set the folder permissions for every one on windows, When I installed the two software, all the information was stored in the Temporary folder,  I changed the permission of temp for everyone according to this link: https://www.softwareok.com/?seite=faq-Windows-7&faq=105
Then I provided permission to TEMP, then ImageMagick and FFmpeg can be installed. 

After downloading, according to the document on their website, I tried resize image and change the format of a video. 
Here is an example: It is sometimes convenient to resize an image as they are read. Suppose you have hundreds of large JPEG images you want to convert to a sequence of PNG thumbails:

magick 333.jpg -resize 120x120 333.png

I changed the format of the video by the command of $ ffmpeg -i 007.mp4 077.avi


![05.png](05.png)
![06.png](06.png)

