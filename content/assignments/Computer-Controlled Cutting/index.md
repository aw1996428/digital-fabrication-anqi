+++
title = "Computer-Controlled-Cutting"
+++

### Design patterns and shape

![77logo.png](77logo.png)

This is my personal logo, I got inspiration from the three initials of my name Wang Anqi.

I planed to make a phone stand with the laser cutting machine,the sketch like this

![61.png](61.png)

I chosed the 4mm wood board, At the begining, Thinking about the kerf at the slot that link the two parts, I cutted exactly the right size for this tab, it was kind of snug, but not suitable for my product,
because the phone stand should have a inclined support surface. It should be more loose for the slot to connect the tab. I set up 3 sets of slots of different heights on a small board.

![62.jpg](62.jpg)

![63.jpg](63.jpg)

The middle slots work good for me, it is 5mm. Then I chosed the MDF Engraving light mode to print my logo.


![64.jpg](64.jpg)
![65.jpg](65.jpg)
![66.jpg](66.jpg)
![617.jpg](617.jpg)
![68.jpg](68.jpg)

To make it can be assembled in mutiple ways, I add one piece to my product so that it can also be used as a bookend.

![69.jpg](69.jpg)
![70.jpg](70.jpg)
![71.jpg](71.jpg)

### Using Fusion 360 to build a box and caculating the kerf



I followed the Finger joint box for laser cutting fusion 360 tutorial.
 https://www.youtube.com/watch?v=ZrcqauNvt0M&t=25s

 ![72.1.png](72.1.png)

This step is about setting all the parameters.

![73.png](73.png)


![74.png](74.png)


![75.png](75.png)

![76.png](76.png)

![77.png](77.png)


![78.png](78.png)

![79.png](79.png)

![80.png](80.png)

![81.png](81.png)

I learned about constraint equality features and quick steps to modify parameters.
The mirror fuction helps me to build only three faces to get a box.

When I export the DXF file and upload it to illustrator, I got two problems.


![811.jpg](811.jpg)

The first one was I realized I have to export three faces separately.

![81.2.png](81.2.png)

The second was the face has many construction line,I don't want it.
So I asked this questions from some classmatea at the fablab, they told me one way to solve it.
I need to build a new sketch for each face and then project the oringinal face to get a clear contour line.

![82.png](82.png)

![83.png](83.png)

![84.png](84.png)


![85.jpg](85.jpg)

The first version on my box was a little bit loose that because I didn't consider the kerf, then I measured it, it was about 0.2mm.


![85.1.jpg](85.1.jpg)

So I changed the parameters of the base, I changed the tab from 15 to 14.75

![86.png](86.png)

Then I got a tight joint box. 

![861.jpg](861.jpg)






