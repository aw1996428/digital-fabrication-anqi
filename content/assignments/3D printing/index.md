+++
title = "3D-Printing"
+++

### 3D print a design rule test object with a FDM printer


I chosed a model thst needed support for testing. 

![1.png](1.png)

For the Build Plate Adhesion Type, There are three different adhesion functions,from the introduction of this video,https://www.youtube.com/watch?v=p4R3F1as-1I, I get to know more about Skirts, Brims and rafts.


All3DP Defines a skirt as "a layer of material laid down on the print bed around and apart from the 3D print"
It is going to trace a certain number of lines around the area where the part is going to be printed afterwards. 

I chosed shirt here so that when the filament to start flowing, it has an area to prime the nozzle. Having a skirt helps me to take a look at a part before it's fully started and make sure nothing should be shifted.

![3.1.jpg](3.1.jpg)

For the Infill part, I chosed the Infill density to 20 and the infill pattern to cubic subdivision,
because the test model doesn't need too strong density. Cubic subdivision was defined as this variation of cubic uses less material.


![3.2.jpg](3.2.jpg)

I added support for this model.

![5.jpg](5.jpg)


![2.jpg](2.jpg)

I learned how to remove support with tools.

![3.jpg](3.jpg)

I also tried printing the bridge without support.

![6.jpg](6.jpg)

![7.jpg](7.jpg)

![8.jpg](8.jpg)

![9.jpg](9.jpg)

![9.1.jpg](9.1.jpg)

While I was printing, there was a slight glitch in the nozzle getting the material, the reason was the problem of wire wrapping, the material gets stuck there, then I rewinded the wire and set the machine back to work with YUHAN's kind help.

The test showed good results. 

The bridge deck did not break from 2 to 20.


### Design an object for 3D printing.

I planed to make a hollow object by Fusion 360. I thought lampshade is good choice.
I learned how to make a lampshade from this auto desk video tutorial. https://www.youtube.com/watch?v=3PnKBSOulwo&t=52s


The basic logic is to make the upper and lower bottom surfaces, then make two curved strips, and then put these two symmetric strips center arrays respectively, and finally obtain a hollow object.

![10.png](10.png)

![11.png](11.png)
The first step was about Halfing the profile and revolving it 

![12.png](12.png)
I chosed the spline tool and add a control point.


![13.png](13.png)

Then I used the revolve command line to create the first solid body 


![14.png](14.png)

Then I split body to two parts.

![15.png](15.png)

To shell the body 2 out to uniform wall thickness

![16.png](16.png)
![17.png](17.png)

I had a nice hollow form for the lampshade.

![18.png](18.png)

I chose extrude and select the profile I just sketched and drag it all the way through and change the direction from one side to symmetric.


![19.png](19.png)

The two symmetric strips that can be pattern around in a circle.

![20.png](20.png)

![21.png](21.png)

Through this experience, I learned how to use fusion to make weaving hollow objects.

### 3D print the model

I tried the SLA machine first, I used a very small size preset, and it took 2 hours for the machine to finish printing. I learned how to use SLA3D printers, how to use cleaning equipment, and drying equipment. 

![22.jpg](22.jpg)

The printed result was not bad, but because it was too small and the support was too difficult to remove, it broke.

![23.jpg](23.jpg)

Finally, I used the Ultimaker printer to print it, I set the print speed to a value of 60, and chose no support, and Brims for the Build Plate Adhesion Type. And I reduced the whole size of the object by half to make it can be printed quickly.

![24.jpg](24.jpg)

![25.jpg](25.jpg)

![26.jpg](26.jpg)

![27.jpg](27.jpg)


{{<video src="28.mp4">}} {{</video>}}



