+++
title = "Computer-Aided Design"
+++

### Introducton
This project is about setting the tangible design in informal learning environment. This work will collaborate with the Botanical Museum (Kasvimuseo). I would like to explore the correlation between tangible design and collaborative learning and also provide an immersive learning experience for the audience. Through the interaction between the visitor and the tangible play, they could know more about the knowledge of Botanical. For instance, how to cultivate different plants, distinguish them and know the poisonous of particular plants. This prototype with a projection, a box and some tangible tokens, audiences can manipulate those token to build their own greenhouse and have a virtual planting experience by controlling temperature, humidity and soil.

![016.jpg](016.jpg)

### Work Flow

The work flow will be build model and animation in blender,then import the blender file into Unity, and then use serial communication to link these different animation effects to different hardware sensors. Such as humidity sensor, temperature sensor, weight sensor and so on.

I followed the tutorial from YouTube
https://www.youtube.com/watch?v=UK0H75pHQ7s&t=609s

![31.png](31.png)

![32.png](32.png)

![33.png](33.png)

![34.png](34.png)

![35.png](35.png)

![36.png](36.png)

![37.png](37.png)

![38.png](38.png)

![39.png](39.png)

![40.png](40.png)


### Making a pot by Fusion 360

This was my first time to use Fusion 360, using real-life parameters, This way is good for me to do future
3D printing work. I spent some time to know about Fusion 360, and followed a tutorial from https://www.youtube.com/watch?v=q528fdf44uM

![f1.png](f1.png)

I understanded the function of loft and -move.

![f2.png](f2.png)



![f3.png](f3.png)
Then I made one piece of the patterns of the planter, I used filet to make the rouned corner.



![f4.png](f4.png)

Then I use circular pattern to make the pattern regularly replicated.


![f5.png](f5.png)





![f6.png](f6.png) 

The shell function to make the pot hollow.




![f7.png](f7.png) 




![f8.png](f8.png) 

Here are some parameters.


![f7.png](f7.png)

Then I finished the edge by filet function.
